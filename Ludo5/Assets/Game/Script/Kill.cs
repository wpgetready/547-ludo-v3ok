﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Kill : MonoBehaviour
{
	public Game game;
	//FZSM: every safe position is relative for each player.
	private int[] safePlace = new int[]{1,9,14,22,27,35,40,48};// this values of the save position in the bord
	public Transform[] player = new Transform[4]; //Initial position of every piece on the game.
												  //This point to a hierarchy, where the childs are the initial positions of every piece.
	public const int FINAL_POSITION = 57; //token at this position should be compared at all.
	public AudioSource audioKill;

	//FZSM: Rebuilt from the ground
	//This first version ignores how many players are playing, so be careful this restriction should be re-examined near future
	//Issue: currentPosition 57 means end of the playing and it should return with no action.
	//In fact, anything above position 51 should need verifkill at all(!)
	//Issue: we don't need to check if the token is in the final position
	public void verifkill (Transform tokenPosition, int playerNumber, int tokenNumber)
	{
		int currentPosition = getTokenPosition (playerNumber, tokenNumber);
		msg ("Player #:" + playerNumber + ",Token# :" + tokenNumber + ", current pos#:" + currentPosition);
		if (currentPosition >51) {
			msg ("Player on final path, verifkill NO ACTION TAKEN!");
			return;
		}
		//Now let's go to the waypoint list and extract the waypoint where the token is placed
		Transform wayPoint = game.wayPointList [playerNumber].GetChild (currentPosition);
		//We can use the name of this waypoint, since is different for EVERY waypoint in the board.

		int piecePos = -1; 		//current piece position
		Transform currWP; 		//current waypoint for the piece we are looking for
		//Make an iteration for every player (0-green,1-yellow,2-blue,3-red)
		for (int i=0; i<4; i++) {
			//if current player matches playerNumber ignore it, we won't compare with ourselves...
			if (i == playerNumber) continue;

			//Let's check where is everybody in relation with the current piece
			//Loop for every token
			for (int j=0; j<4; j++) {
				piecePos = getTokenPosition (i, j); //Where is the token?
				msg ("Checking... Player #:" + i + ",Piece #:" + j + ", Pos #:" + piecePos);
				//RULE: if the piece is in a safe place, ignore this token and Go to the next one
				if (Array.IndexOf (safePlace, piecePos) != -1){
					msg ("Piece is safe, skipping...");
					continue;
				}

				//RULE: if the piece is over position 51, not check is necesary.
				if (piecePos>51) {
					msg ("Piece is on final path, skipping...");
					continue;
				}

				currWP = game.wayPointList [i].GetChild (piecePos);
				//If the pieces are in the same waypoint, reset the piece
				if (wayPoint.name == currWP.name) {
					msg ("We found a coincidence!!!");
					//i is the playerNumber, j is the token for the playerNumber
					resetToken(piecePos,getPlayerPiece(i,j),i,player[i].GetChild (j)); //Make effect moving piece from current pos backwards to zero.
					resetPiecePosition(i,j); 										   //Reset the token position to zero.
				}
			}
		}
	}

	/// <summary>
	/// Gets the player piece.
	/// Given a player (0...3) and a token (0...3) returns the token object to move in the UI
	/// </summary>
	/// <returns>The player piece.</returns>
	/// <param name="player">Player.</param>
	/// <param name="token">Token.</param>
	private Transform getPlayerPiece(int player,int token) {
		Transform [] currT=null;
		switch (player) {
		case 0:
			currT= game.p1;break;
		case 1:
			currT= game.p2;break;
		case 2:
			currT= game.p3;break;
		case 3:
			currT= game.p4;break;
		default:
			msg ("ERROR: incorrect play value: " + player);
			break;
		}
		return currT[token];
	}

	/// <summary>
	/// Resets the piece position.
	/// This is going to be improved in future version. It has no much sense having 4 arrays.
	/// </summary>
	/// <param name="playerNumber">Player number.</param>
	/// <param name="tokenNumber">Token number.</param>
	private void resetPiecePosition(int playerNumber,int tokenNumber) 
	{
		switch (playerNumber) {
		case 0:
			game.pp1 [tokenNumber]=0;break;
		case 1:
			game.pp2 [tokenNumber]=0;break;
		case 2:
			game.pp3 [tokenNumber]=0;break;
		case 3:
			game.pp4 [tokenNumber]=0;break;
		}
	}
	
	/// <summary>
	/// Given a player number (0..3) and a token (0..3) return the position of that token (0...57)
	/// Clearly this need to be improved in future iterations
	/// </summary>
	/// <param name="">.</param>
	private int getTokenPosition (int playerNumber, int tokenNumber)
	{
		switch (playerNumber) {
		case 0:
			return game.pp1 [tokenNumber];
		case 1:
			return game.pp2 [tokenNumber];
		case 2:
			return game.pp3 [tokenNumber];
		case 3:
			return game.pp4 [tokenNumber];
		default:
			Debug.LogError ("ERROR: Kill.verifkill: invalid playerNumber " + playerNumber);
			return -1;
		}
	}

	
	//FZSM:changed name for something more meaningul. kill was overkill.
	/// <summary>
	/// Resets the token to the initial position, walking back from the current position toward cell zero.
	/// </summary>
	/// <param name="initialPosition">Initial position.</param>
	/// <param name="tokenPos">Token position.</param>
	/// <param name="affectedPlayer">Affected player(0..3)</param>
	/// <param name="init">Cell zero where the final position is the token placed.</param>
	public void resetToken (int initialPosition, Transform tokenPos, int affectedPlayer, Transform zeroPosition)// function return the token kill in the init position
	{
		audioKill.Play ();
		StartCoroutine (returnToCellZero (initialPosition, tokenPos, affectedPlayer, zeroPosition));
	}
	
	//FZSM: from the current position, the token goes back to the initial pos, walking back all the waypoints.
	IEnumerator returnToCellZero (int initialPosition, Transform tokenPos, int affectedPlayer, Transform zeroPosition)// from is position to 0 positon that he out of it
	{
		Transform targetWayPoint;
		for (int i =initialPosition-1; i >=0; i--) {
			yield return new WaitForSeconds (0.05f);
			targetWayPoint = game.wayPointList [affectedPlayer].GetChild (i);
			tokenPos.position = targetWayPoint.position; 
			if (i == 0) {
				tokenPos.position = zeroPosition.position;
			}
		} 
		audioKill.Pause ();
	}

	private void msg(string msg) {
		Debug.Log(msg);
	}
}

//20170207- currently 146 lines.
//20170208 - 170 lines, but code is way, way better and documented.

