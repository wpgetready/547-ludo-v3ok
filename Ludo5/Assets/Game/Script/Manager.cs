﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public enum ManagerStates
{
	noState, gameCompleted, levelFailed, sceneRestart,sceneExit
}

public class Manager : MonoBehaviour {
	
	public static event System.Action<ManagerStates> ManagerStateChanged = delegate { };
	private ManagerStates managerState = ManagerStates.noState; //We start the class with noState at all.
	public ManagerStates adState
	{
		get
		{
			return managerState;
		}
		private set
		{
			managerState = value;
			ManagerStateChanged (managerState);
		}
	}
	
	public Game game;
	public Player player;
	public Win win;
	public Vspc vsps;
	public Text V1,V2;

	public string[] listplayer =new string[2];
	public int playerCount; 	//FZSM: stands for how many players are playing in one game. (2,3 or 4)
	public int currentPlayer=0; 		//FZSM:num stands for the next player to move.
	public GameObject PanelEnd;
	private TextMeshProUGUI txtWinner;
	public GameObject txtWinnerGameObject; //Define text to put when winner ends.
	
	public const string NO_WINNER_YET="?";
	
	public string winTracker; //tracks who win in a mutiple player round
	
	void Start () {
		PanelEnd.SetActive(false);
		listplayer[0]=V1.text;// take type of game humaine or vs computer 
		listplayer[1]=V2.text;// take number of the player they wuill play 
		int.TryParse(V2.text, out playerCount);
		adState = ManagerStates.sceneRestart; //fire event, launch scene, for admob banner.
		Debug.Log("Manager: Start Event");
		txtWinner = txtWinnerGameObject.GetComponent<TextMeshProUGUI> ();
		winTracker = NO_WINNER_YET;
	}
	
	public bool isTurnForPlayer(int c)// verification to tour 
	{
		return (c == currentPlayer);
	}
	
	/// <summary>
	/// Automatiquetour the specified c.
	/// Given a Player, it will check if ANY token of the player can move with the dice value provided.
	/// This check will ignore any token in the initial position (0).
	/// If it find at least ONE position we can move using the dice value it will return true.
	/// </summary>
	/// <param name="c">C.</param>
	public bool isMovAvailable(int c)// function return true if one or more token of the player when we add to his positon not initial <57
	{   
		switch (c)
		{
		case 0:
			for(int i =0; i <4 ; i++)
			{
				if(game.pp1[i]!=0&&game.pp1[i]+game.diceValue<=57) return true;
			}
			return false;
			
		case 1:
			for(int i =0; i <4 ; i++)
			{
				if(game.pp2[i]!=0&&game.pp2[i]+game.diceValue<=57) return true;
			}
			return false;
			
		case 2:
			for(int i =0; i <4 ; i++)
			{
				if(game.pp3[i]!=0&&game.pp3[i]+game.diceValue<=57) return true;
			}
			return false;
			
		case 3:
			for(int i =0; i <4 ; i++)
			{
				if(game.pp4[i]!=0&&game.pp4[i]+game.diceValue<=57) return true;
			}
			return false;
		}
		return false;
	}

	void twoPlayers ()
	{
		if (endGameCondition ()) {
			if (win.playerWon [0]) {
				txtWinner.SetText ("Player Green Wins!");
			}
			else {
				txtWinner.SetText ("Player Blue Wins!");
			}
			PanelEnd.SetActive (true);
			Time.timeScale = 0;
			return;
		}
		//No end condition, switch currentPlayer
		currentPlayer = (currentPlayer == 0) ? 2 : 0;
	}

	void humanVsCPU ()
	{
		if (endGameCondition ()) {
			if (win.playerWon [0]) {
				txtWinner.SetText ("Human Green Wins!");
			}
			else {
				txtWinner.SetText ("Computer Blue Wins!");
			}
			PanelEnd.SetActive (true);
			Time.timeScale = 0;
			return;
		}
		//No ending condition
		if (currentPlayer == 0) {
			currentPlayer = 2;
			vsps.selectCpuPlayer (currentPlayer);
			return;
		}
		//currentPlayer IS 2
		currentPlayer = 0;
		player.cpuTurn = false;
		game.canRoll = true;
	}

	void trackWinner ()
	{
		if (winTracker == NO_WINNER_YET) {
			bool Cpu = (listplayer [0] != "VSPer");
			if (win.playerWon [0]) {

				winTracker =Cpu?"Human Green":"Green";
			}
			else
				if (win.playerWon [1]) {
					winTracker =Cpu?"CPU Yellow":"Yellow";
				}
				else
					if (win.playerWon [2]) {
						winTracker =Cpu?"CPU Blue":"Blue";
					}
					else
						if (win.playerWon [3]) {
							winTracker =Cpu?"CPU Red": "Red";
						}
		}
	}


	//switch turns

	/// <summary>
	/// Play turn, plays current turn and move to the next turn if needed
	/// </summary>
	public void playTurn ()// function that change the  number num and change the tourn beteween the player
	{
		game.canRoll = true;
		game.canMoveTokens = true;
		
		if (listplayer [0] == "VSPer")  //If there are only players
		{
			if (playerCount == 2) {
				twoPlayers ();  return;
			} 
			currentPlayer++; //Current player>2 (and less than 4)
			trackWinner (); //Track the first winner
			if (endGameCondition()) {
				txtWinner.SetText("Player " + winTracker + " Wins!");
				PanelEnd.SetActive (true);
				Time.timeScale = 0;
				return;
			} 
			//No end condition, if the current player already won, go to the next turn
			if (win.playerWon[currentPlayer]) {
				playTurn ();
				return;
			}
			//Current player didn't win, switch the currentPlayer index if we need
			if (currentPlayer+1 > playerCount) {
				currentPlayer = 0;
				if (win.playerWon[0]) { //if player won, change the tour
					playTurn ();
					return;
				}
			}
			return; 
		} 
		
		//This section is human against CPU
		if (playerCount == 2) {
			humanVsCPU ();  return;
		} 
		currentPlayer++; 	//Player count is >2 (3 or 4)
		trackWinner(); 		//Track the first winner
		
		if (endGameCondition()) {
			txtWinner.SetText(winTracker + " Wins!");
			print ("end game");
			PanelEnd.SetActive (true);
			Time.timeScale = 0;
			return;
		} 
		//No winning condition. Does the current player won?
		if (win.playerWon[currentPlayer]) {
			playTurn (); //yes go to next turn
			return;
		} 
		
		if (currentPlayer+1 > playerCount) { //switch player index if needed.
			currentPlayer = 0;
			if (win.playerWon[0]) {
				playTurn ();
				return;
			} 
			player.cpuTurn = false; //No player didn't win yet
			return;
		} 
		//No need to reset
		vsps.selectCpuPlayer (currentPlayer);// computer turn to play
	}

	
	/// <summary>
	/// Checks end gaming condition which is: if there are n-1 players which ended, the game ends.
	/// </summary>
	/// <returns><c>true</c>, if game condition was ended, <c>false</c> otherwise.</returns>
	private bool endGameCondition() {
		int c1, c2, c3, c4;
		//If there are two players, currently they are 0 and 2
		//If there are three players, currently they are 0,1 and 2
		//If there are four players, they will be 0...3
		switch (playerCount) {
		case 2: //Two players (human vs. human or human vs cpu)
			if (win.playerWon[0]) return true;
			if (win.playerWon[2]) return true;
			return false;
		case 3: //three players : 3 humans or 1 vs 2 cpu
			c1 = (win.playerWon[0])?1:0;
			c2 = (win.playerWon[1])?1:0;
			c3 = (win.playerWon[2])?1:0;
			return (c1+c2+c3>=2);
			
		case 4: //four players: 4 humans or 1 h vs 3 cpu
			c1 = (win.playerWon[0])?1:0;
			c2 = (win.playerWon[1])?1:0;
			c3 = (win.playerWon[2])?1:0;
			c4 = (win.playerWon[3])?1:0;
			return (c1+c2+c3+c4>=3);
		}
		return false;
	}
	
	public void Menu(){
		Time.timeScale=1;
		adState = ManagerStates.sceneExit; //notify that we are exiting game Scene, to destroy banner.
		SceneManager.LoadScene("main");
	}
	
}

//20190210: 303 lines
// shrink to 273