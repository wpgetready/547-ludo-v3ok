﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour
{
	// Use this for initialization
	public GameObject Panel1, Panel2, Panel3, PanelOn, PanelOf;
	public Text num;
	private int i = 2;
	public string[] lp = new string[2];
	public Text V1, v2;
     
	void Start ()
	{
		Time.timeScale = 1;
		num.text = i.ToString ();
		lp [1] = i.ToString ();
	}

	public	void Button ()
	{    
		if (EventSystem.current.currentSelectedGameObject.name == "VS Pc") {
			lp [0] = "VSPc";
			Panel2.SetActive (false);
			Panel1.SetActive (true);

		} else if (EventSystem.current.currentSelectedGameObject.name == "VS Per") {
			lp [0] = "VSPer";
			Panel3.SetActive (false);
			Panel1.SetActive (true);
		} else if (EventSystem.current.currentSelectedGameObject.name == "Retur") {
			Panel1.SetActive (false);
			Panel2.SetActive (true);
			Panel3.SetActive (true);
		} else if (EventSystem.current.currentSelectedGameObject.name == "Play") {
			V1.text = lp [0].ToString ();//take the type of the game VS humaine Or vs computer 
			v2.text = lp [1].ToString ();// take the number of the players 
			SceneManager.LoadScene ("game");
		} else if (EventSystem.current.currentSelectedGameObject.name == "+") {
			if (i < 4)
				i += 1;
			num.text = i.ToString ();
			lp [1] = i.ToString ();

		} else if (EventSystem.current.currentSelectedGameObject.name == "-") {
			if (i > 2)
				i -= 1;
			num.text = i.ToString ();
			lp [1] = i.ToString ();
	 
		} else if (EventSystem.current.currentSelectedGameObject.name == "SoundOn") {
			PanelOf.SetActive (true);
			PanelOn.SetActive (false);
			AudioListener.volume = 0f;
		} else if (EventSystem.current.currentSelectedGameObject.name == "SoundOf") {
			PanelOn.SetActive (true);
			PanelOf.SetActive (false);
			AudioListener.volume = 1f;
		}
	}
}