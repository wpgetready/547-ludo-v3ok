﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
	//FZSM: Player token position: every array contains: 4 elements (4 tokens for each player)
	//The RELATIVE position of each element from its start: When the player starts, every token is in the zero position .
	//This value changes from 0 to 57 (last position).
	//Every value is relative to their initial position: I mean the pos 1 for green player is different from pos 1 player blue. 
	public int[] pp1 = new int[4];//four nnumber of the position  token of the player one  green 
	public int[] pp2 = new int[4];//four nnumber of the position  token of the player tow yellow  
	public int[] pp3 = new int[4];//four nnumber of the position  token of the player three bleau  
	public int[] pp4 = new int[4];//four nnumber of the position  token of the player four red  
    
	public int diceControl = 0;
	public bool canRoll = true;  //Flag for saying if the player can roll the dice or not (dice will be hidden/show)
	public bool initial = false;
	public bool canMoveTokens = false;//Flag for saying if the player can touch the tokens.

	//verif ==> verification that he comite bounce and move to allouer change tour /
	public Transform[] postour = new Transform[6];// four transform of th efour position of the dice 
	public GameObject Dice;//dice
	private Image face;//face dice 
	public Sprite[] diceFaces = new Sprite[6];//6 face of the dice
	public int diceValue = 1;// face dice number of the dice 
	 
	public Transform[] wayPointList = new Transform[4];// four waypoint to the four players
	 
	Transform targetWayPoint;// the next positon that the token move to it

	public Transform[] p1 = new Transform[4];// the four token of the first player green 
	public Transform[] p2 = new Transform[4];// the four token of the second player yellow 
	public Transform[] p3 = new Transform[4];// the four token of the thred player bleau  
	public Transform[] p4 = new Transform[4];// the four token of the fourest player red 
	public Transform[] scal1 = new Transform[4];// the four images of the tokens of the player green 
	public Transform[] scal2 = new Transform[4];// the four images of the tokens of the player yellow
	public Transform[] scal3 = new Transform[4];// the four images of the tokens of the player bleau 
	public Transform[] scal4 = new Transform[4];// the four images of the tokens of the player red
	public Manager manager;
	public Kill kill;
	public Win win;
	public AudioSource audioDICE, audioJump, audioJump1;

	void Start ()
	{
		initial = false;
		Dice.transform.position = postour [0].transform.position;
		face = Dice.GetComponent<Image> ();
		face.sprite = diceFaces [Random.Range (0, 6)];
	}

	/// <summary>
	/// Automatiquescale the specified c.
	/// Automatic scale is used as follows:
	/// Every time a player is going to move, it is realzed just making it a bit bigger (50%), and making the other smaller.
	/// This method should be improved with animations.
	/// </summary>
	/// <param name="c">C.</param>
	public void reScaleTokens (int c)// change the scale of the 4 token when they have they tourn  else rest in there scale
	{   
		switch (c) {
		case 0:
			for (int i = 0; i < scal1.Length; i++) {
				scal1 [i].localScale = new Vector3 (1.5f, 1.5f, 1);
				scal2 [i].localScale = new Vector3 (1, 1, 1);
				scal3 [i].localScale = new Vector3 (1, 1, 1);
				scal4 [i].localScale = new Vector3 (1, 1, 1);
			}
			break;

		case 1:
			for (int i = 0; i < scal1.Length; i++) {
				scal1 [i].localScale = new Vector3 (1, 1, 1);
				scal2 [i].localScale = new Vector3 (1.5f, 1.5f, 1);
				scal3 [i].localScale = new Vector3 (1, 1, 1);
				scal4 [i].localScale = new Vector3 (1, 1, 1);
			}
			break;

		case 2:
			for (int i = 0; i < scal1.Length; i++) {
				scal1 [i].localScale = new Vector3 (1, 1, 1);
				scal2 [i].localScale = new Vector3 (1, 1, 1);
				scal3 [i].localScale = new Vector3 (1.5f, 1.5f, 1);
				scal4 [i].localScale = new Vector3 (1, 1, 1);
			}
			break;

		case 3:
			for (int i = 0; i < scal1.Length; i++) {
				scal1 [i].localScale = new Vector3 (1, 1, 1);
				scal2 [i].localScale = new Vector3 (1, 1, 1);
				scal3 [i].localScale = new Vector3 (1, 1, 1);
				scal4 [i].localScale = new Vector3 (1.5f, 1.5f, 1);
			}
			break;
		}
	}
	
	private void Update ()
	{
		// change the position of the dice depening of the tour 
		Dice.transform.position = postour [manager.currentPlayer].transform.position;
		reScaleTokens (manager.currentPlayer);// change the scale depending of the turn 
	
	}
	//FZSM: changed to simplify kill process and make it independent of board size
	//Refactored variables to something meaningfull.
	public  int moveplayer (int currentPlayerPosition, Transform tokenPosition, int playerNumber, int tokenNumber)// moving the player
	{
								// w is the player number green =0, yellow =1, bleau =2, red=3
		canRoll = false;		// disable the button of the bounce dice 
		canMoveTokens = false;	// disable pressing to the token 

		//if the player is the initial position he will be in the posiiton 1. Pos 0 is the initial position until the dice rolls a 6.
		if (currentPlayerPosition == 0) {
			//We move one position. Don't call delayJump, since is a overkill, but basically do the EXACT same operation.
			audioJump1.Play ();
			targetWayPoint = wayPointList [playerNumber].GetChild (0);
			tokenPosition.position = targetWayPoint.position;
			canRoll = true;
			currentPlayerPosition = 1;
			return  currentPlayerPosition;
		} else {// else he follow the numbere of his dice
			audioJump.Play ();
			StartCoroutine (delayJump (currentPlayerPosition, tokenPosition, playerNumber, tokenNumber));
		}

		currentPlayerPosition = currentPlayerPosition + diceValue;
		return  currentPlayerPosition;
	}

	/// <summary>
	/// Main routine for advancing piece. The strategy is breaking the process in one-step advances, displaying the token and playing a sound
	/// every time the token repositions it.
	/// </summary>
	/// <returns>The jump.</returns>
	/// <param name="currentPlayerPosition">Current player position.</param>
	/// <param name="tokenPosition">Token position.</param>
	/// <param name="playerNumber">Player number.</param>
	/// <param name="tokenNumber">Token number.</param>
	IEnumerator delayJump (int currentPlayerPosition, Transform tokenPosition, int playerNumber, int tokenNumber)
	{
		for (int i =currentPlayerPosition; i <currentPlayerPosition+diceValue; i++) {
			canRoll = false;
			yield return new WaitForSeconds (0.15f);
			targetWayPoint = wayPointList [playerNumber].GetChild (i);
			tokenPosition.position = targetWayPoint.position; 
		} 
		canRoll = true;
		canMoveTokens = false;
		audioJump.Pause ();
		kill.verifkill (tokenPosition, playerNumber,tokenNumber);/// verification if he when he after his moving he kill one of the other tokens
		win.verifwin (playerNumber);// verification that he win or not 
	}

	/// <summary>
	/// Rolls the dice. Is this explanatory enough? :)
	/// </summary>
	public void rollTheDice ()
	{   
		if (canRoll) {
			//Stop any interactions before rolling
			canMoveTokens = false;
			canRoll = false;
			float i = 0;
			StartCoroutine (customDelay ());
			//Repeat this process at least 5 times, creating an effect of rolling dice...
			while (i <0.8f) {
				i += 0.15f; //0.15 * 5 =0.75<0.8f
				StartCoroutine (spinDice (i));
			}
		}
	}

	IEnumerator customDelay ()
	{
		//play rolling dice sound for 1.1 seconds (notice this is bigger than the time of making dice rolling about 0.8 seconds)
		audioDICE.Play ();
		yield return new WaitForSeconds (1.1f);
		audioDICE.Pause ();
		diceValue ++; //Increment dice value (otherwise we would end up in a 0..5 value)

		canMoveTokens = true;// the player can press on his token after he rolling the dice 
		if (!manager.isMovAvailable (manager.currentPlayer) && diceValue != 6 &&  initial   ) {
			//HINDI if he not in first tour and he don't have not option to play we call to change the tour 
			Debug.Log("COND 1 MET");
			StartCoroutine (wait ());
		} else if (manager.isMovAvailable (manager.currentPlayer) && diceValue != 6 && initial) {
			// he have a option to play it 
			Debug.Log("COND 2 MET");
			canMoveTokens = true;

		} else if (!initial && diceValue != 6) {
			//HINDI he is int he first tour and he don't have any option we call to change the tour
			Debug.Log("COND 3 MET");
			manager.currentPlayer = 0;
			initial = true;
			StartCoroutine (wait ());
			 
		} else if (!initial && diceValue == 6) {
			//HINDI the same but he have dice 6 we lat it play
			Debug.Log("COND 4 MET");
			manager.currentPlayer = 0;
			canMoveTokens = true;
		}
	}
       
	/// <summary>
	/// Spins the dice.
	/// Pay attention this 'spinning' is half of the job. 
	/// </summary>
	/// <returns>The dice.</returns>
	/// <param name="i">The index.</param>
	IEnumerator spinDice (float i)
	{
		diceValue = Random.Range (0, 6);      	//Select a value between 0 and 5 (not six)
		face.sprite = diceFaces [diceValue]; 	//Select corresponding dice face
		yield return new WaitForSeconds (i / 2);//Wait 
		diceValue = setDiceValue ();        	//repeat the process, but this time instantaneous. 
		face.sprite = diceFaces [diceValue];
	}

	IEnumerator wait ()
	{
		canRoll = false;
		canMoveTokens = false;
		yield return new WaitForSeconds (1f);
		manager.playTurn ();
	}	

	/// <summary>
	/// This method is introduced to control dice value, for testing purposes. Otherwise, testing the app is hard and random.
	/// </summary>
	/// <returns>The dice value.</returns>
	private int setDiceValue() {
		if (diceControl != 0) {
			if (diceControl<0) {
				diceControl=1;
			} else if (diceControl>6) {
				diceControl=6;
			}
			return diceControl-1;
		} else {
			return Random.Range(0,6);
		}
	}
}
//20190207- 230 initial line count -- to 192
