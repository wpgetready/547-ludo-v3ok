﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


//20190201: Adapting code for Ludo Game. How difficult can be use the decoupling concept which I implemented in Stickman Draft?
//In fact, adapting was really easy, but we need to account that an event should be installed on every object needed to control Admob
//Aside that, custom enumeration are also recommended for documentation purposes (we can ignore it, but the code will be easier to read)
//This app proves also we can work with object that are NOT singleton.
public class DelegateManager : MonoBehaviour {

    public static DelegateManager instance;
	public AudioSource tokenReached;
    AdmobStates currAdState = AdmobStates.noState;
    ManagerStates currUIState = ManagerStates.noState;
	PlayerStates currPlayerState = PlayerStates.noState;

    private Manager manager;
    private AdmobManager admob;

    bool flagInterstitial = false;



    // Use this for initialization
    void Start () {
        if (!instance)
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }

    }
	
	void Update () {}

    void OnEnable()     //Suscribe to the events, of every object needed
    {
        Manager.ManagerStateChanged += Manager_ManagerStateChanged;
        AdmobManager.AdmobStateChanged += AdmobManager_AdmobStateChanged;
		Player.PlayerStateChanged += Player_PlayerStateChanged;
    }

    void OnDisable() //Unsuscribe to the events when disabled
    {
        Manager.ManagerStateChanged -= Manager_ManagerStateChanged;
        AdmobManager.AdmobStateChanged -= AdmobManager_AdmobStateChanged;
		Player.PlayerStateChanged -= Player_PlayerStateChanged;
    }


	private void Player_PlayerStateChanged(PlayerStates obj) {
		currPlayerState = obj;
		switch (obj) {
		case PlayerStates.tokenReachedEnd:
			AdmobManager.instance.ShowInterstitial ();
			Debug.Log ("Interstitial fired!");
			break;
		default:break;
		}
	}

    private void AdmobManager_AdmobStateChanged(AdmobStates obj)
    {
        currAdState = obj;
        //Filter here unneeded events
        Debug.Log("FZ:DelegateManager AdmobManager Fired event:" + obj.ToString());
        switch (obj)
        {
            case AdmobStates.noState:
                break;
            case AdmobStates.interClosed:
                //Interstitial is closed, it is a good time to request another one.
                if (flagInterstitial)
                {
                    AdmobManager.instance.RequestInterstitialNormal(); //Normal Interstitial
                    Debug.Log("FZ: DM asking form Normal Interstitial");
                } else
                {
                    AdmobManager.instance.RequestInterstitialVideo(); //Video Interstitial
                    Debug.Log("FZ: DM asking for Video Interstitial");
                }
                flagInterstitial = !flagInterstitial;
			//FZSM: Little trick but it can work MAYBE in the future:
			//Currently we know for sure that interstitial are displayed right befor a token reach final position
			//So when intersitial is closed is valid play a winning sound.
			//There is a problem: in editor mode DOES NOT work, since google ads does not work also...
			//20190210: Tested , but it didn't work properly
			tokenReached.Play();
                break;
            case AdmobStates.videoRewarded:
                break;
            case AdmobStates.videoClosed:
                break;
            default:
                break;
        }
    }

    private void Manager_ManagerStateChanged(ManagerStates obj)
    {
        switch (obj)
        {
            case ManagerStates.noState:
                break;
            case ManagerStates.gameCompleted:
                break;
            case ManagerStates.levelFailed:
                break;
            case ManagerStates.sceneRestart: //Restart Admob banner
                if (AdmobManager.instance == null)
                {
                    Debug.LogError("ERROR: AdmobManager.instance null!");
                }
                else
                {
                    AdmobManager.instance.RequestBanner(GoogleMobileAds.Api.AdPosition.Bottom);
					AdmobManager.instance.RequestInterstitialNormal();
                    Debug.Log("Banner requested");
                }
                break;
		case ManagerStates.sceneExit:
			//If we exit from game scene, the banner is destroyed.
			//Business rule: don't show banner on the main screen.
			AdmobManager.instance.DestroyBanner();
			break;
            default:
                break;
        }

    }

   
    private void fireInterstitial()
    {
        AdmobManager.instance.ShowInterstitial(); //I had some unexplainable error, so I took it out from coroutine.
        //StartCoroutine(showAdsDelayed(0.5f));
        //Reset both counters,since we already displayed an interstitial.
        Debug.Log("FZ:Interstitial fired");
    }

}