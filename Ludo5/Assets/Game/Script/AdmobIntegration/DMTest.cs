﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//This class is a DelegateManager test to check operation about AdmobManager and any DelegateManager

public class DMTest : MonoBehaviour {

    public GameObject WhereIsAdmobManager;
    public GameObject WhereIsAdmobCaller;
    private AdmobManager am;
    private AdmobCallerTest ac;

    // Use this for initialization
    void Start()
    {
        am = WhereIsAdmobManager.GetComponent<AdmobManager>();
        ac = WhereIsAdmobCaller.GetComponent<AdmobCallerTest>();
        Debug.Log("DMTest Started");
    }


    void OnEnable()
    {
        //Connect to the events. Questions: if I have an AdmobManager object, this fires the events here or what
        //Answer: at first, it should. I cannot refere AdmobManager_AdmobStateChanged from an instnciated object for example.
        //I'll have to test it out...
        //Shit.
        AdmobManager.AdmobStateChanged += AdmobManager_AdmobStateChanged;
        AdmobCallerTest.CommandList += AdmobCallerTest_CommandList;
    }

    private void AdmobCallerTest_CommandList(Commands obj)
    {
        switch (obj)
        {
            case Commands.noState:
                break;
            case Commands.requestBanner:
                am.RequestBanner(GoogleMobileAds.Api.AdPosition.Bottom);
                Debug.Log("Banner Requested and auto-displayed");
                break;

            case Commands.destroyBanner:
                am.DestroyBanner();
                Debug.Log("Banner destroyed");
                break;

            case Commands.requestInterstitial:
                am.RequestInterstitialNormal();
                Debug.Log("Interstitial Requested");
                break;

            case Commands.showInterstitial:
                am.ShowInterstitial();
                Debug.Log("Interstitial Displayed");
                break;

            case Commands.requestVideo:
                am.RequestRewardBasedVideo();
                Debug.Log("Requested Video!");
                break;


            case Commands.showVideo:
                am.ShowRewardBasedVideo();
                Debug.Log("Video Displayed");
                break;
            default:
                break;
        }
    }

    //Unsuscribe to the events
    void OnDisable()
    {
        AdmobManager.AdmobStateChanged -= AdmobManager_AdmobStateChanged;
        AdmobCallerTest.CommandList -= AdmobCallerTest_CommandList;
    }


    private void AdmobManager_AdmobStateChanged(AdmobStates obj)
    {
        switch (obj)
        {
            case AdmobStates.noState:
                break;
            case AdmobStates.interClosed:
                break;
            case AdmobStates.videoRewarded:
                break;
            case AdmobStates.videoClosed:
                break;
            default:
                break;
        }
    }
}
