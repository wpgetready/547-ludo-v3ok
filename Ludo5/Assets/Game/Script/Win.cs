﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Again, class made from scratch. There is so many mistakes in here.
/// </summary>
public class Win : MonoBehaviour {
public Game game;
public Manager manager;
//public int rang=0;
		//,w1=0,w2=0,w3=0,w4=0;// variable to verification 
public bool[] playerWon = new bool[4];

public GameObject[] wingamepanel=new GameObject[4];// win panel of every player
public Text[] range=new Text[4];
	// Use this for initialization
    //FZSM: defines who won in the game. While is zero, nobody won.
	//if w1=1, or w2=2 or w3=3 or w4=4, they won.
	//Until now I don't know WHY in the Earth needs to be different for every play.
	//Wouldn't be enough wi different than zero?
	 void Start() {
		/*
		w1=0;
		 w2=0;
		 w3=0;
		 w4=0;
		 */
		playerWon[0]= false;
		playerWon[1]= false;
		playerWon[2]= false;
		playerWon[3]= false;

	}
	public void verifwin(int c)// verification if the number of player win or not
	{
        checkPlayerWins(c); 
		if (playerWon[c]){
			range[c].text=(++c).ToString();
			manager.playTurn();
		}
/*
        switch (c)
        {
        case 0:
		if(w1==1)//if he win we pass to other turn of player 
		{
			 rang+=1;
			 range[0].text=(rang).ToString();
           manager.tour();
		}
		

		break;
		case 1:
		if(w2==2)
		{ rang+=1;
			 range[1].text=(rang).ToString();
         manager.tour();
		}
		
		break;
		case 2:
		if(w3==3)
		{
			 rang+=1;
			 range[2].text=(rang).ToString();
         manager.tour();
		}
		
		break;
		case 3:
		if(w4==4)
		{
			rang+=1;
			 range[3].text=(rang).ToString();
         manager.tour();
		}
		break;
		}
		*/
	}

    //FZSM: Cleaned up a bit
	public void checkPlayerWins (int c)// verification if all the token of the player in the fianl position
	{
		switch (c)
        {
        case 0:
			if (checkWinCondition(game.pp1)==4)
            {
                //w1 =1;
				playerWon[0]=true;
                wingamepanel[0].SetActive(true);
            }
            break;

       case 1:
			if(checkWinCondition(game.pp2)==4)
            {
                //w2=2;
				playerWon[1]=true;
                wingamepanel[1].SetActive(true);
            }
            break;

            case 2:
			if (checkWinCondition(game.pp3)==4)
            {
                //w3=3;
				playerWon[2] = true;
                wingamepanel[2].SetActive(true);
            }
            break;

		case 3:
			if (checkWinCondition(game.pp4)==4)
            {
                //w4=4;
				playerWon[3]=true;
                wingamepanel[3].SetActive(true);
            }
            break;
		}
	}

    //Return number of token that reached position
    //IF token==4 this is a win condition
    public int checkWinCondition (int[] tokenPositions)
    {
        int tokensWon = 0;
        for (int i = 0; i < 4; i++)
        {
            if (tokenPositions[i] == 57)
            {
                tokensWon++;
            }
        }
        return tokensWon;
    }
}
