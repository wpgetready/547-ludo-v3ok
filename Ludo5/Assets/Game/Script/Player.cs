using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum PlayerStates
{
	noState, tokenReachedEnd,playerWon,PlayerFailed
}

/// <summary>
/// 20190210- Refactored Player to make it meaningful, documented and UNDERSTANDABLE.
/// Even though, there is many tasks to do to make a clean and functional code.
/// 20190210 - Generalized. Created humanBot and cpuBot, both completely functional. humanBot is a subset
///  (simplification) of cpuBot
/// </summary>
	public class Player : MonoBehaviour {
	public Game game;
	public int tokenNumber;// number if token
	public char typ1,typ2;
	public Manager manager;
	public GameObject buttonbouncedive;// object button bounce dice

	public bool cpuTurn = false;			// true if cpu is playing
	public bool cpuCanPlayAgain=false;      // true if cpu can play again (usually when dice rolls to 6)
	public bool cpuCanMoveOtherToken=false; // true if cpu can move another token (and usually current token can't move)

	public static event System.Action<PlayerStates> PlayerStateChanged = delegate { };
	private PlayerStates playerState = PlayerStates.noState; //We start the class with noState at all.
	public PlayerStates playingState
	{
		get
		{
			return playerState;
		}
		private set
		{
			playerState = value;
			PlayerStateChanged (playerState);
		}
	}

	void Update()
	{
		if(cpuTurn&&manager.currentPlayer!=0)
		{
				buttonbouncedive.SetActive(false);//desable the boutton of bounce dice for the humain player
		}
		else if(!cpuTurn&&manager.currentPlayer==0)
		{
			buttonbouncedive.SetActive(true);
		}
	}

	public void buttonmov()//function when pressing on a token 
	{   
		string playerColorInitials = "gybr"; //Color ordering: green,yellow,blue,red
		int playerNumber = 0;
		if (!cpuTurn) {//when is it not the turn of the computer we use typ1 and type2 a variable
			string typ = EventSystem.current.currentSelectedGameObject.name;// name of the button we press 
			typ1 = typ [0];// type1 take the alpahbet g,b,r,y =yellow ,green , red,bleau 
			typ2 = typ [1];//type2 take the number of token 
			tokenNumber = (int)char.GetNumericValue (typ2);// convert from char to int
		
			//Take notice that just only this function would ride all the switch case under here....
			//FZSM:If we don't have playerNumber, we are in serious trouble.
			if (playerNumber == -1) {
				Debug.LogError ("FUNDAMENTAL ERROR: unable to find playerNumber when moving the pieces!!!");
			}
		} 
		//FZSM: Directly get player number to use later in collision and kill
		playerNumber = playerColorInitials.IndexOf (typ1);

		switch (typ1)
        {
	        case 'g':
				humanBot(playerNumber,game.pp1,game.p1);break;
	        case 'y':
				cpuBot(playerNumber,game.pp2,game.p2)  ;break;
			case 'b':
				cpuBot(playerNumber,game.pp3,game.p3); break;
	        case'r':
				cpuBot(playerNumber,game.pp4,game.p4); break;
        }
    }

	/// <summary>
	/// Cpu Bot (intelligence against human player)
	/// Calling example cpuBot(2, game.pp3, game.p3)
	/// </summary>
	/// <param name="playerNumber">Player number (0=green,1=yellow,2=blue,3=red</param>
	/// <param name="tokenPos">Array of Token positions</param>
	/// <param name="tokenTrf">Array of Token Transformations</param>
	void cpuBot(int playerNumber, IList<int> tokenPos,IList<Transform> tokenTrf){

		if (!manager.isTurnForPlayer(playerNumber)) return;
		if (!game.canMoveTokens) return;
		bool movesAvailable = manager.isMovAvailable(playerNumber);
		
		if(tokenPos[tokenNumber]==0)//If we are in zero position (initial position)
		{
			if(game.diceValue==6) //RULE: a 6 is rolled, move the player to initial position and let the cpu play again.
			{
				tokenPos[tokenNumber]=game.moveplayer(tokenPos[tokenNumber],tokenTrf[tokenNumber],playerNumber,tokenNumber);
				cpuCanPlayAgain=true;
				cpuCanMoveOtherToken=false;
				return;
			}
			//dice value isn't 6, check if there are available moves
			if(movesAvailable) //there are at least one move,flag it properly. Currently I don't know why game.canRoll needs to be false.
			{
				game.canRoll=false;
				cpuCanPlayAgain=false;
				cpuCanMoveOtherToken=true;
				return;
			}
			//No moves, flag it properly and pass to next turn (Question: why I need to flag it if I'm passing to other turn?)
			cpuCanPlayAgain=false;
			cpuCanMoveOtherToken=false;
			StartCoroutine(passNextTurn());
			return;
		}
		//From here we know that token is NOT zero position
		if(game.diceValue==6) //If we got a 6
		{
			if(tokenPos[tokenNumber]+game.diceValue<=57) //Value is under the max?then move the token and allow play it again
			{
				tokenPos[tokenNumber]=game.moveplayer(tokenPos[tokenNumber],tokenTrf[tokenNumber],playerNumber,tokenNumber);
				cpuCanPlayAgain=true;
				cpuCanMoveOtherToken=false;
				return;
			}
			//The value is over the max, is there any other mov available or other token in zero position?
			if(movesAvailable|tokenPos[0]==0|tokenPos[1]==0|tokenPos[2]==0|tokenPos[3]==0)
			{
				//Yes, then you can't play again but you can move other token (?). No conocia esta regla.
				//Maybe I should examine the rules again.
				game.canRoll=false;
				cpuCanPlayAgain=false;
				cpuCanMoveOtherToken=true;
				return;
			}
			//There isn't another move available. Deactivate and pass to next turn
			cpuCanPlayAgain=false;
			cpuCanMoveOtherToken=false;
			StartCoroutine(passNextTurn());
			return;
		}
		//from here token is NOT in zero position, and dice value is NOT 6. 
		cpuCanPlayAgain = false;
		if(tokenPos[tokenNumber]==57)// is this token already in the goal?
		{
			game.canRoll=false; //cpuCanPlayAgain=false;
			cpuCanMoveOtherToken=true; //allow to test another tokens
			return;
		}  
		
		if(tokenPos[tokenNumber]+game.diceValue<=57) //can we move? Yes, then move it and pass to next turn
		{
			tokenPos[tokenNumber]=game.moveplayer(tokenPos[tokenNumber],tokenTrf[tokenNumber],playerNumber,tokenNumber);
			cpuCanMoveOtherToken=false;//cpuCanPlayAgain=false;
			StartCoroutine(passNextTurn());
			return;
		}
		//we can't move. Is there another mov available?
		cpuCanMoveOtherToken = movesAvailable;
		if(movesAvailable) //yes, allow to move
		{
			game.canRoll=false;	//cpuCanPlayAgain=false;//cpuCanMoveOtherToken=true;
			return;
		}
		//No other moves available, pass to next turn 	//cpuCanPlayAgain=false; //cpuCanMoveOtherToken=false;
		StartCoroutine(passNextTurn());
		return;
	}

	/// <summary>
	/// Human player main loop. This is a simplified flow of cpuBot, without cpu flags
	/// Calling example cpuBot(0, game.pp1, game.p1)
	/// </summary>
	/// <param name="playerNumber">Player number (0=g,1=y,2=b,3=r)</param>
	/// <param name="tokenPos">Array of Token positions</param>
	/// <param name="tokenTrf">Array of Token Transformations</param>
	private void humanBot(int playerNumber, IList<int> tokenPos,IList<Transform> tokenTrf){
		
		if(!manager.isTurnForPlayer(0)) return;
		if(!game.canMoveTokens) return; 
		bool movesAvailable = manager.isMovAvailable(playerNumber);
		
		if(tokenPos[tokenNumber]==0) //if we are in the initial position
		{
			if (game.diceValue==6) //and we roll a 6
			{
				//Move the token (and play again)
				tokenPos[tokenNumber]=game.moveplayer(tokenPos[tokenNumber],tokenTrf[tokenNumber],playerNumber,tokenNumber);
				return;
			}
			//dice value is less than 6
			if(!movesAvailable)//Are there any moves available?
			{
				StartCoroutine(passNextTurn()); //Nope, concede the turn
				return;
			}
			//There are other moves available, go to other token
			return;
		}
		
		//We are not in the initial position  tokenPos[tokenNumber]!=0 is always true
		
		if (game.diceValue==6) //and we roll a 6
		{ 
			//Can we move?
			if(tokenPos[tokenNumber]+game.diceValue<=57)
			{
				//Yes , move it (and play again)
				tokenPos[tokenNumber]=game.moveplayer(tokenPos[tokenNumber],tokenTrf[tokenNumber],playerNumber,tokenNumber);//normal movement
				return;
			}
			//We can't move. Is there a move available or some other token in the initial position?
			//20190210: Found a 'behavior' here:
			//If we got a six on the final ride and it result is above, the game keeps waiting
			//until we touch the token.
			//This behavior happens because movesAvailable should look for available moves in the OTHER tokens
			//but current movesAvailable looks for ALL tokens (including current) which returns true.
			//This is a special condition requiring improve isMovAvailable function
			//and taking in account currentToken for excluding this case.

			if(movesAvailable|tokenPos[0]==0|tokenPos[1]==0|tokenPos[2]==0|tokenPos[3]==0)
			{
				game.canRoll=false;				//Yes, re-iterate to check the other tokens.
				return;
			}
			StartCoroutine(passNextTurn()); //We can't move and also there is no other options available. Concede this turn.
			return;
		}
		//dice value is less than 6 and also we are NOT in zero position
		if(tokenPos[tokenNumber]==57)// Are we in the end?
		{
			game.canRoll=false; //Yes, try another token
			return;
		}          
		
		if(tokenPos[tokenNumber]+game.diceValue<=57) //Can we move?
		{
			//Yes, let's move it, and concede this turn (since we had a value less than 6)
			tokenPos[tokenNumber]=game.moveplayer(tokenPos[tokenNumber],tokenTrf[tokenNumber],playerNumber,tokenNumber);
			StartCoroutine(passNextTurn());
			//FZSM:After moving, check if the token reached end zone and fire interstitial (delayed 2 seconds)
			if (tokenPos[tokenNumber]==57 ){
				Debug.Log ("Interstitial Ad delayed...");
				StartCoroutine(delayAd(2));
			}
			return;
		}
		//Values is above 57, is there any move available (from other token, not this current)
		if(movesAvailable)
		{
			game.canRoll=false; //Yes , check another token
			return;
		}
		StartCoroutine(passNextTurn());//No moves, concede the turn
		return;			 
	}

	 IEnumerator passNextTurn()
	 {   
		game.canRoll=false;
		game.canMoveTokens=false;
		cpuTurn=false;
		yield return new WaitForSeconds(1f);
		manager.playTurn();
	 }	

	IEnumerator delayAd(float seconds)
	{ 
		yield return new WaitForSeconds(seconds);
		playingState= PlayerStates.tokenReachedEnd;
	}
 }