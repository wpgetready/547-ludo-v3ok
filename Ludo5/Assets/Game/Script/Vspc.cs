using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vspc : MonoBehaviour {
public Game game;
public Player player;


/// <summary>
/// Pc the specified c.
	/// This is the 'intelligent bot' behind game playability.
	/// As you will quickly see, there is nothing 'intelligent' in here.
/// </summary>
/// <param name="c">C.</param>
	public void selectCpuPlayer(int c) {
		 player.cpuCanPlayAgain=true;
		 player.cpuCanMoveOtherToken=true;
 		 player.cpuTurn=true;
		//FZSM:Selecting a random token means that no intelligence in the token selection process.
		//Therefore, random leads the running, not a strategy
		player.tokenNumber=(Random.Range(0,4));

		switch (c)
        {
		 case 1:
		 player.typ1='y'; break;
	
        case 2:
		 player.typ1='b'; break;

		case 3:
		 player.typ1='r'; break;
	    }
		game.canRoll=true;
		game.rollTheDice();//reuse function button dice 
		StartCoroutine(cpuMove(c));
	}

	/// <summary>
	/// Decides how to move acording rules on buttonmov
	/// </summary>
	/// <returns>The move.</returns>
	/// <param name="m">M.</param>
	 IEnumerator cpuMove(int m)
	 {
		 yield return new WaitForSeconds(1.5f);
		 player.buttonmov();//play buttonmov like a normal user.
		  
		while(!player.cpuCanPlayAgain&&player.cpuCanMoveOtherToken&&game.canMoveTokens)//in this condition he has other chance 
		{
			player.cpuTurn=true;
			game.canMoveTokens=true;
			player.tokenNumber=(Random.Range(0,4)); //Select a random token to play
			player.buttonmov();
			print("cond1");
		}
		 if(player.cpuCanPlayAgain&&!player.cpuCanMoveOtherToken)//in this condition he has other chance and he will have another turn and other bounce dice 
		{
			 StartCoroutine(waitMSeconds(m));
			 player.cpuCanPlayAgain=true;
		     player.cpuCanMoveOtherToken=true;
			 print("cond2");
		}
		else if(!player.cpuCanPlayAgain&&!player.cpuCanMoveOtherToken)// in this condition he has not chance to play 
		{
			player.cpuTurn=false;
			//game.can=true;
			//game.verif=fals
			player.cpuCanPlayAgain=true;
		    player.cpuCanMoveOtherToken=true;
			print("cond3");

		}
		   
	 }
	 IEnumerator waitMSeconds(int m)
	 {
		 yield return new WaitForSeconds(1f);
		 selectCpuPlayer(m);
	 }
}
